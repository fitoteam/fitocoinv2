package fitocoin;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.PublicKey;

/**
 * @author Tiago Stapenhorst Martins
 * @author Felipe Ramon de Lara
 */
public class EntityBean implements Serializable {

    //protected PublicKey pkey;
    protected PublicKey pkey;
    protected String name;
    protected InetAddress ip;
    protected int unicastPort;

    public PublicKey getPkey() {
        return pkey;
    }

    public void setPkey(PublicKey pkey) {
        this.pkey = pkey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public InetAddress getIp() {
        return ip;
    }

    public void setIp(InetAddress ip) {
        this.ip = ip;
    }

    public void setIp(String ip) throws UnknownHostException {
        this.ip = InetAddress.getByName(ip);
    }

    public int getUnicastPort() {
        return unicastPort;
    }

    public void setUnicastPort(int unicastPort) {
        this.unicastPort = unicastPort;
    }

    public void printEntityBean() {
        WindowLogger.get().addLogLine(this.getEntityBeanString());
    }

    public String getEntityBeanString() {
        if (this.ip == null) {
            WindowLogger.get().addLogLine("Ip is null");
        }
        if (this.pkey == null) {
            WindowLogger.get().addLogLine("Pkey is null");
        }
        if (this.name == null) {
            WindowLogger.get().addLogLine("Name is null");
        }
        return "Entity Bean\nName: " + this.name + "\nIP: " + this.getIp().toString() + "\nUnicast port: " + this.unicastPort + "\nPublic Key: " + this.pkey.toString();
    }
    
    public boolean equals(EntityBean eb){
        return eb.pkey.equals(this.pkey);
    }
}
