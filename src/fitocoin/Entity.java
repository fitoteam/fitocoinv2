package fitocoin;

import fitocoin.gui.EntitiesJListModel;
import fitocoin.gui.PaymentRecordsTable;
import java.io.IOException;
import java.io.Serializable;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import org.apache.commons.lang3.SerializationUtils;

/**
 * @author Tiago Stapenhorst Martins
 * @author Felipe Ramon de Lara
 */
public final class Entity extends EntityBean {

    protected KeyPair keys;
    protected SecretKey secretKey;
    protected Thread unicastListener;
    protected Thread multicastListener;
    protected EntitiesJListModel entityPool;
    protected PaymentRecordsTable paymentRecords; //our payments database
    private MulticastSocket multicastSocket;
    private DatagramSocket unicastSocket;
    protected Log ownLog;
    protected boolean isLocked = false;

    /**
     * Entity Constructor, intializes some important variables in important
     * orders, be careful.
     *
     * @param name
     * @param port
     * @param entities
     * @param prt
     */
    public Entity(String name, int port, EntitiesJListModel entities, PaymentRecordsTable prt) {
        WindowLogger.get().addLogLine("Setting ip automatically");
        this.setIpAutomatically();
        WindowLogger.get().addLogLine("Setting other stuff");
        this.name = name;
        this.unicastPort = port;
        this.entityPool = entities;
        this.paymentRecords = prt;
        WindowLogger.get().addLogLine("Setting multicast listener");
        this.setMulticastListener();
        WindowLogger.get().addLogLine("Setting unicast listener");
        this.setUnicastListener();

        try {
            //Inicializador da chave simétrica
            KeyGenerator keyGen;
            keyGen = KeyGenerator.getInstance(Constants.SYMMETRIC_CYPHER_ALGORITHM);
            keyGen.init(128);
            secretKey = keyGen.generateKey();
            WindowLogger.get().addLogLine("Symetric key generated.");

            //Inicializador da chave assimétrica
            KeyPairGenerator kpg = KeyPairGenerator.getInstance(Constants.ASYMMETRIC_CYPHER_ALGORITHM);
            kpg.initialize(Constants.CRYPTO_KEY_SIZE);
            this.keys = kpg.generateKeyPair();
            this.pkey = keys.getPublic();
            WindowLogger.get().addLogLine("Assymetric key pair generated.");

            //Inicia o socket multicast e se conecta ao grupo
            multicastSocket = new MulticastSocket(Constants.MULTICAST_PORT);
            multicastSocket.joinGroup(Constants.getMulticastAddress());

            //Inicia o socket unicast
            unicastSocket = new DatagramSocket(unicastPort);

            //Inicia os listeners, multicast e unicast
            this.multicastListener.start();
            this.unicastListener.start();

            //Mensagem envia seu EntityBean via multicast para que os usuarios o reconheçam na rede
            this.sendHelloMessage();

        } catch (NoSuchAlgorithmException | IOException ex) {
            Logger.getLogger(Entity.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Entity(EntityFileData entityFileData, EntitiesJListModel entities, PaymentRecordsTable prt) {
        this.keys = entityFileData.keys;
        this.secretKey = entityFileData.secretKey;
        this.setIpAutomatically();
        this.name = entityFileData.entity.name;
        this.pkey = entityFileData.entity.pkey;
        this.unicastPort = entityFileData.entity.unicastPort;
        this.entityPool = entities;
        this.paymentRecords = prt;

        WindowLogger.get().addLogLine("Setting multicast listener");
        this.setMulticastListener();
        WindowLogger.get().addLogLine("Setting unicast listener");
        this.setUnicastListener();

        try {
            //Inicia o socket multicast e se conecta ao grupo
            multicastSocket = new MulticastSocket(Constants.MULTICAST_PORT);
            multicastSocket.joinGroup(Constants.getMulticastAddress());

            //Inicia o socket unicast
            unicastSocket = new DatagramSocket(unicastPort);

            //Inicia os listeners, multicast e unicast
            this.multicastListener.start();
            this.unicastListener.start();

            //Mensagem envia seu EntityBean via multicast para que os usuarios o reconheçam na rede
            this.sendHelloMessage();

        } catch (IOException ex) {
            Logger.getLogger(Entity.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setIpAutomatically() {
        try {
            this.ip = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            Logger.getLogger(Entity.class.getName()).log(Level.SEVERE, null, ex);
            this.ip = null;
        }
    }

    private void setUnicastListener() {
        this.unicastListener = new Thread() {
            @Override
            public void run() {
                this.setName("Unicast Listener");

                try {
                    byte[] buffer = new byte[Constants.BYTE_ARRAY_SIZE];
                    while (true) {
                        WindowLogger.get().addLogLine("Creating datagram packet for unicast listener.");
                        DatagramPacket request = new DatagramPacket(buffer, buffer.length);
                        WindowLogger.get().addLogLine("Receiving unicast message");
                        unicastSocket.receive(request);
                        WindowLogger.get().addLogLine("Dealing with unicast message");
                        dealWithMessage((Message) SerializationUtils.deserialize(request.getData()));
                    }
                } catch (SocketException e) {
                    WindowLogger.get().addLogLine("Socket: " + e.getMessage());
                } catch (IOException e) {
                    WindowLogger.get().addLogLine("IO: " + e.getMessage());
                }
            }
        };
    }

    private void setMulticastListener() {
        this.multicastListener = new Thread() {
            @Override
            public void run() {
                this.setName("Multicast Listener");
                WindowLogger.get().addLogLine("Multicast receiver n.o " + this.getId() + " started.");
                try {
                    int count = 1;
                    do {
                        //Cria o buffer para guardar a mensagem recebida.
                        byte[] buffer = new byte[Constants.BYTE_ARRAY_SIZE];
                        DatagramPacket dp = new DatagramPacket(buffer, buffer.length);
                        multicastSocket.receive(dp);
                        String output = "Datagram " + count + " received from: " + dp.getSocketAddress();
                        WindowLogger.get().addLogLine(output);
                        //Deserialize object
                        Message message = (Message) SerializationUtils.deserialize(buffer);
                        WindowLogger.get().addLogLine("Deserializing multicast message.");
                        dealWithMessage(message);
                        count++;
                    } while (!false //(messageIn.getData()).equals(("Exit").getBytes())
                            );
                    //s.leaveGroup(group);
                    //WindowLogger.get().addLogLine("Receiver finished.");
                } catch (IOException ex) {
                    Logger.getLogger(Entity.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
    }

    /**
     * Trata as mensagens recebidas pela rede Cada mensagem tem um tratamento
     * específico diferente, filtrado pelo switch case
     *
     * @param message
     */
    public void dealWithMessage(Message message) {
        byte[] decryptedSecretKeyByte;
        SecretKey secretKeyTemp;
        byte[] decryptedOfferByte;
        Payment offer;

        WindowLogger.get().addLogLine(message.getType().toString() + " message received from " + message.getOrigin().name + ".");

        if (this.getBean().getPkey().equals(message.getOrigin().getPkey())) {
            //Ignore message because it has been sent by the present Entity
            return;
        }
        switch (message.getType()) {
            case HELLO:
                /*
                1)Salva a EntityBean do rementente no EntityPool
                2)Envia seu PaymentRecords (banco de dados) ao remetente
                 */
                //1):
                entityPool.add(message.getOrigin());
                WindowLogger.get().addLogLine("Eu, " + this.name + " recebi uma atualização em minha EntityPool:" + entityPool.getEntityPoolString());
                //responder em unicast o proprio bean e PaymentRecords (update newbie)
                Message answer = new Message(Constants.MessageType.PAYMENT_RECORDS, this.getBean(),
                        SerializationUtils.serialize((Serializable) this.paymentRecords.getRecords()));
                sendMessageUnicast(message.getOrigin().getIp(), message.getOrigin().getUnicastPort(), answer);
                //salva a lista recebida do usuário no seu PaymentRecords
                EntityFileData data = (EntityFileData) SerializationUtils.deserialize(message.getBinaryObject());
                this.paymentRecords.addPaymentsThatCouldBeRedundant(data.ownTransactions);
                break;
            case PAYMENT_RECORDS:
                /*
                1) Adiciona o remetente da mensagem no EntityPool
                2) Verifica se o PaymentRecords está vazio
                3) Atualiza o seu PaymentRecords
                 */
                this.entityPool.add(message.getOrigin());

                if (this.paymentRecords.getRecords().isEmpty()) {
                    //Deserializa PaymentRecords recebido pela mensagem e o substitui
                    this.paymentRecords.setRecords((List<Payment>) SerializationUtils.deserialize(message.getBinaryObject()));
                    String output = "";
                    output = output.concat("Eu, " + this.name + ", recebi "
                            + Constants.MessageType.PAYMENT_RECORDS.toString()
                            + " de " + message.getOrigin().getName() + "\n");
                    output = output.concat("Novo PaymentRecords:\n");
                    output = output.concat(this.paymentRecords.getPaymentsContent());
                    WindowLogger.get().addLogLine(output);
                } else {
                    //Deserializa PaymentRecords recebido pela mensagem e insere os pagamentos que não são redundantes
                    List<Payment> currentPaymentsList = (List<Payment>) SerializationUtils.deserialize(message.getBinaryObject());
                    this.paymentRecords.addPaymentsThatCouldBeRedundant(currentPaymentsList);
                }
                break;
            case PAYMENT_OFFER:
                decryptedSecretKeyByte = Encryption.asymmetricDecryptMessage(message.getOrigin().getPkey(), message.getEncryptedSecretKey());
                secretKeyTemp = (SecretKey) SerializationUtils.deserialize(decryptedSecretKeyByte);
                WindowLogger.get().addLogLine("Chave simétrica recebida:" + secretKeyTemp.toString());

                decryptedOfferByte = Encryption.symmetricDecrypt(message.getBinaryObject(), secretKeyTemp);
                offer = (Payment) SerializationUtils.deserialize(decryptedOfferByte);
                WindowLogger.get().addLogLine("Eu, " + this.name + ", recebi " + Constants.MessageType.PAYMENT_OFFER.toString() + " no valor de " + offer.getValue() + " de " + message.getOrigin().getName() + "\n");

                //Pergunta ao usuário se ele aceita o pagamento ou não.
                //Se aceitar, envia uma mensagem VERIFY_PAYMENT em Multicast para que
                //as entidades do grupo verifiquem os fundos daquele pagamento.
                //Se negar, apenas não envia nada
                if (!isLocked) { //askForPaymentAccept()

                    isLocked = true;
                    WindowLogger.get().addLogLine("Estou travando minha carteira pois aceitei a oferta de pagamento.");

                    //Prossegue com transferência
                    //enviar pagamento com a criptografia do pagador
                    //entao mensagem deve ter bean do pagador
                    message.setType(Constants.MessageType.VERIFY_PAYMENT);
                    this.sendMessageMulticast(message);
                } else {
                    Message reply = new Message(Constants.MessageType.BUSY, this.getBean());
                    this.sendMessageUnicast(message.getOrigin().getIp(), message.getOrigin().getUnicastPort(), reply);
                    WindowLogger.get().addLogLine("Você declarou estar ocupado para novos pagamentos");
                }
                break;
            //lidar case reply
            //destravar bean quando apropriado

            case BUSY:
                WindowLogger.get().addLogLine("" + message.getOrigin().name + " declarou que está ocupado com outra transação. Tente novamente mais tarde.");
                //Destravar carteira pois transação não irá acontecer
                this.isLocked = false;
                WindowLogger.get().addLogLine("Estou destravando minha carteira porque meu pagamento foi recusado.");
                break;
            case VERIFY_PAYMENT:
                /*
                Após receber um unicast oferecendo um pagamento, o receptor do pagamento
                envia em multicast uma solicitação para aquele pagamento seja verificado
                (se há dinheiro na conta ou não, etc...). Esta é a mensagem recebida.
                Então os que receberam por multicast irão minerar (verificar) esse pagamento
                e retornar se o pagamento foi validado para o receptor do pagamento.
                 */
                decryptedSecretKeyByte = Encryption.asymmetricDecryptMessage(message.getOrigin().getPkey(), message.getEncryptedSecretKey());
                secretKeyTemp = (SecretKey) SerializationUtils.deserialize(decryptedSecretKeyByte);
                decryptedOfferByte = Encryption.symmetricDecrypt(message.getBinaryObject(), secretKeyTemp);
                offer = (Payment) SerializationUtils.deserialize(decryptedOfferByte);
                //Se o pagamento estiver com esta entidade envolvida, ele não faz nada
                if (offer.getPayee().getName().equals(this.name) || offer.getPayer().getName().equals(this.name)) {
                    //Não faz nada, o pagamento veio ou é para esta entidade.
                } else if (this.paymentRecords.payerHasEnoughFunds(offer)) {
                    WindowLogger.get().addLogLine("Eu, " + this.name + ", recebi " + Constants.MessageType.VERIFY_PAYMENT.toString() + " no valor de " + offer.getValue() + " de " + message.getOrigin().getName() + "\n");
                    Payment verifiedPayment = offer;
                    verifiedPayment.setTimeWhenVerifiedByMiner(new java.util.Date());
                    verifiedPayment.setMiner(this.getBean());
                    Message msg = new Message(Constants.MessageType.PAYMENT_VERIFIED, this.getBean(), SerializationUtils.serialize(verifiedPayment));
                    sendMessageUnicast(verifiedPayment.getPayer().getIp(), verifiedPayment.getPayee().getUnicastPort(), msg);
                } else {
                    //Não faz nada, nao existem fundos na carteira para a transacao.
                }
                break;
            case PAYMENT_VERIFIED:
                /*
                O usuario que aceitou o pagamento receberá esta mensagem via unicast.
                Ele verificara se aquele pagamento ja foi registrado em PaymentRecords.
                Se não tiver registrado, via multicast a ação de salvar aquele pagamento no PaymentRecords
                Salva no seu PaymentRecords o pagamento
                 */

                Payment verifiedPayment = (Payment) SerializationUtils.deserialize(message.getBinaryObject());
                if (this.paymentRecords.paymentAlreadyExist(verifiedPayment)) {
                    //Pagamento já existe, não faz nada
                } else {
                    //1) Adiciona payment ao payment records
                    //2) Notifica em multicast o novo pagamento
                    //3) Cria e adiciona premio do minerador ao payment records
                    //4) Notifica em multicast o prêmio do minerador
                    //1)
                    this.paymentRecords.addVerifiedPayment(verifiedPayment);
                    //2)
                    Message registerPayment = new Message(Constants.MessageType.RECORD_PAYMENT, this.getBean());
                    registerPayment.setBinaryObject(message.getBinaryObject());
                    WindowLogger.get().addLogLine("Eu, " + this.name + ", recebi " + Constants.MessageType.PAYMENT_VERIFIED.toString() + " no valor de " + verifiedPayment.getValue() + " de " + message.getOrigin().getName() + "\n");

                    sendMessageMulticast(registerPayment);
                    //3)
                    Payment reward = new Payment(null, new java.util.Date(), verifiedPayment.getMiner(), verifiedPayment.getPayer(), null, Constants.MINING_REWARD, Constants.PaymentType.REWARD);

                    Message registerReward = new Message(Constants.MessageType.RECORD_REWARD, this.getBean());
                    registerReward.setBinaryObject(SerializationUtils.serialize(reward));
                    this.paymentRecords.addVerifiedPayment(reward);
                    //4)                    
                    sendMessageMulticast(registerReward);

                    this.isLocked = false;
                    WindowLogger.get().addLogLine("Estou destravando a carteira pois recebi o pagamento verificado do minerador.");
                }

                break;

            case RECORD_PAYMENT:
                /*
                Salva o Payment recebido no PaymentRecord
                 */

                Payment payment = (Payment) SerializationUtils.deserialize(message.getBinaryObject());

                this.paymentRecords.addVerifiedPayment(payment);

                if (payment.getPayer().equals(this.getBean())) {
                    //Recebendo aviso de pagamento efetivado de umas das próprias transações
                    //Deve registrar o pagamento e destravar a carteira
                    this.isLocked = false;
                    WindowLogger.get().addLogLine("Estou destravando minha carteira pois recebi via multicast que o pagamento foi efetuado.");
                }
                break;

            case RECORD_REWARD:
                /*
                Salva o Payment recebido no PaymentRecord
                 */

                Payment paymentReward = (Payment) SerializationUtils.deserialize(message.getBinaryObject());

                this.paymentRecords.addVerifiedPayment(paymentReward);
                WindowLogger.get().addLogLine("Registrei a recompensa de "+paymentReward.getPayer().name+" para "+paymentReward.getPayee().name);
                break;

            case GOODBYE:
                /*
                1) Remove o remetente da mensagem do EntityPool
                 */
                WindowLogger.get().addLogLine("Eu, " + this.name + ", recebi " + Constants.MessageType.GOODBYE.toString() + " de " + message.getOrigin().getName() + ".\n");
                this.entityPool.removeElement(message.getOrigin().getName());
                WindowLogger.get().addLogLine(this.entityPool.getEntityPoolString());
                break;
            default:
                WindowLogger.get().addLogLine("Tipo de mensagem desconhecida.");
                break;
        }
    }

    public void sendMessageUnicast(InetAddress ip, int port, Message message) {
        WindowLogger.get().addLogLine("Unicast Sender Started.");
        try {
            byte[] m = SerializationUtils.serialize(message);
            DatagramPacket dp
                    = new DatagramPacket(m, m.length, ip, port);
            unicastSocket.send(dp);
        } catch (SocketException e) {
            WindowLogger.get().addLogLine("Socket: " + e.getMessage());
        } catch (IOException e) {
            WindowLogger.get().addLogLine("IO: " + e.getMessage());
        }
    }

    /**
     * Send the message given via multicast
     */
    public boolean sendMessageMulticast(Message message) {
        try {
            WindowLogger.get().addLogLine("Sending " + message.getType() + " message via multicast.");
            message.getOrigin().getName();
            //message.printEntityBean();
            WindowLogger.get().addLogLine("Serializing message.");
            byte[] data = SerializationUtils.serialize(message);
            WindowLogger.get().addLogLine("Sending packet to multicast group.");
            multicastSocket.send(new DatagramPacket(data, data.length, Constants.getMulticastAddress(), Constants.MULTICAST_PORT));
            WindowLogger.get().addLogLine("Multicast " + message.getType() + " message sent.");
        } catch (UnknownHostException ex) {
            Logger.getLogger(Entity.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Entity.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public EntityBean getBean() {
        EntityBean eb = new EntityBean();
        eb.ip = this.ip;
        eb.name = this.name;
        eb.pkey = this.pkey;
        eb.unicastPort = this.unicastPort;

        return eb;
    }

    public void sendMyEntityBeanUnicast() {

    }

    public void sendExitMessage() {
        //Manda mensagem falando que está se desligando do grupo.
        //Pede para que seu EntityBean seja removido da EntityPool dos usuários do grupo
        Message byeMessage = new Message(Constants.MessageType.GOODBYE, getBean());
        sendMessageMulticast(byeMessage);
    }

    public void sendHelloMessage() {
        //Manda uma mensagem informando sua entrada no grupo, seu EntityBean e solicita o banco de dados atual de transações.
        EntityFileData data = new EntityFileData(this);
        data.keys = null;
        data.ownLog = null;
        data.secretKey = null;

        byte[] dataByte = SerializationUtils.serialize(data);

        Message newMessage = new Message(Constants.MessageType.HELLO, getBean(), dataByte);
        sendMessageMulticast(newMessage);
    }

    public void sendPaymentOffer(int value, EntityBean destination) {
        Payment paymentOffer = new Payment(null, new java.util.Date(), destination, this.getBean(), null, value, Constants.PaymentType.NORMAL);

        //Trava a carteira
        this.isLocked = true;
        WindowLogger.get().addLogLine("Estou travando minha carteira pois iniciei um pagamento.");

        Message newMessage = new Message(Constants.MessageType.PAYMENT_OFFER, this.getBean());
        byte[] encriptedPayment = Encryption.symmetricEncrypt(SerializationUtils.serialize(paymentOffer), this.secretKey);
        newMessage.setBinaryObject(encriptedPayment);

        newMessage.setEncryptedSecretKey(Encryption.asymmetricEncryptMessage(this.keys.getPrivate(), SerializationUtils.serialize(this.secretKey)));

        sendMessageUnicast(destination.getIp(), destination.getUnicastPort(), newMessage);
    }

    //Not being used
    public void acceptPaymentOffer(Message paymentMessage) {
        //Envia para multicast o pagamento para que os mineiradores o verifiquem
        paymentMessage.setType(Constants.MessageType.VERIFY_PAYMENT);
        sendMessageMulticast(paymentMessage);
    }

    private boolean askForPaymentAccept() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void poolData() {
        WindowLogger.get().addLogLine("Eu, " + this.name + " possuo " + this.entityPool.getSize() + " entidades registradas em meu pool no momento.");
    }

    public PaymentRecordsTable getPaymentRecordsTable() {
        return this.paymentRecords;
    }

}
