/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitocoin.gui;

import fitocoin.EntityBean;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 *
 * @author Tito
 */
public class EntitiesJListModel extends AbstractListModel {

    private final List<EntityBean> entities;

    public EntitiesJListModel() {
        this.entities = new ArrayList<>();
        ListDataListener lis = new ListDataListener() {
            @Override
            public void intervalAdded(ListDataEvent e) {
            }

            @Override
            public void intervalRemoved(ListDataEvent e) {
            }

            @Override
            public void contentsChanged(ListDataEvent e) {
            }
        };
        super.addListDataListener(lis);
        super.removeListDataListener(lis);
    }

    @Override
    public int getSize() {
        return entities.size();
    }

    @Override
    public Object getElementAt(int index) {
        return entities.get(index).getName();
    }

    public boolean add(EntityBean newEntity) {
        for (EntityBean bean : entities) {
            if (bean.getName().equals(newEntity.getName())) {
                return false;
            }
        }
        if (this.entities.add(newEntity)) {
            fireContentsChanged(this, 0, getSize());
            return true;
        } else {
            return false;
        }
    }

    public EntityBean getEntity(int index) {
        return entities.get(index);
    }

    public boolean removeElement(EntityBean eb) {
        boolean removed = entities.remove(eb);
        if (removed) {
            fireContentsChanged(this, 0, getSize());
        }
        return removed;
    }

    public EntityBean searchElementByName(String name) {
        for (EntityBean eb : this.entities) {
            if (eb.getName().equals(name)) {
                return eb;
            }
        }
        return null;
    }

    public boolean removeElement(String toRemoveName) {
        EntityBean toRemove = searchElementByName(toRemoveName);
        if (toRemove != null) {
            boolean removed = entities.remove(toRemove);
            if (removed) {
                fireContentsChanged(this, 0, getSize());
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public String getEntityPoolString() {
        String out = "EntityPool has " + this.entities.size() + " entities.";
        for (int i = 0; i < this.entities.size(); i++) {
            //out = out.concat("\n" + this.entities.get(i).getEntityBeanString());
            out = out.concat("\n" + this.entities.get(i).getName());
        }
        return out;
    }
}
