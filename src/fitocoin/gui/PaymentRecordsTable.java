/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitocoin.gui;

import fitocoin.Constants;
import fitocoin.EntityBean;
import fitocoin.Payment;
import fitocoin.WindowLogger;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Tito
 */
public class PaymentRecordsTable extends AbstractTableModel {

    private List<Payment> records;

    public PaymentRecordsTable() {
        this.records = new ArrayList<>();
    }

    public static enum NomeColunas {
        PAYER(0, "Payer"),
        PAYEE(1, "Payee"),
        VALUE(2, "Value"),
        TIME_CREATED(3, "Time Created"),
        TIME_MINED(4, "Time Mined"),
        MINER(5, "Miner"),
        TYPE(6, "Type");

        public int indice;
        public String nome;

        NomeColunas(int val, String nom) {
            this.indice = val;
            this.nome = nom;
        }

        public String getName() {
            return nome;
        }

        public int getIndex() {
            return indice;
        }
    }

    @Override
    public String getColumnName(int columnIndex) {
        return NomeColunas.values()[columnIndex].nome;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }

    @Override
    public int getRowCount() {
        return this.records.size();
    }

    @Override
    public int getColumnCount() {
        return NomeColunas.values().length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                //Payer
                return this.records.get(rowIndex).getPayer().getName();
            case 1:
                //Payee
                return this.records.get(rowIndex).getPayee().getName();
            case 2:
                //Value
                return this.records.get(rowIndex).getValue();
            case 3:
                //Time Created
                return this.records.get(rowIndex).getTimeWhenCreated();
            case 4:
                //Time Mined
                Object timeMined = this.records.get(rowIndex).getTimeWhenVerifiedByMiner();
                if (timeMined == null) {
                    return "";
                } else {
                    return timeMined;
                }
            case 5:
                //Miner
                EntityBean miner = this.records.get(rowIndex).getMiner();
                if (miner == null) {
                    return "";
                } else {
                    return miner.getName();
                }
            case 6:
                //Type
                return this.records.get(rowIndex).getPaymentType().toString();
            default:
                return "Error!";
        }
    }

    public void addVerifiedPayment(Payment newPayment) {
        if (newPayment.getPaymentType() == Constants.PaymentType.NORMAL) {
            if (newPayment.isVerified()) { //newPayment.isVerified()
                this.records.add(newPayment);
            } else {
                WindowLogger.get().addLogLine("Payment not verified.");
            }
        } else if (newPayment.getPaymentType() == Constants.PaymentType.REWARD) {
            this.records.add(newPayment);
            fireTableDataChanged();
        }
    }

    public void addPaymentThatCouldBeRedundant(Payment newPayment) {
        if (!paymentAlreadyExist(newPayment)) {
            this.records.add(newPayment);
            fireTableDataChanged();
        }
    }

    public void addPaymentsThatCouldBeRedundant(List<Payment> newPayments) {
        for(Payment p : newPayments){
            if(p!=null){
                addPaymentThatCouldBeRedundant(p);
            }
        }
    }

    public List<Payment> getRecords() {
        return records;
    }

    public void setRecords(List<Payment> records) {
        this.records = records;
        fireTableDataChanged();
    }

    public String getPaymentsContent() {
        String output = "";
        output = output.concat("Payment Records array size:" + this.records.size());
        for (int i = 0; i < this.records.size(); i++) {
            output = output.concat("\n" + this.records.get(i).getPaymentsContent());
        }
        return output.concat("\nFim do Payment Records.");
    }

    public void printPaymentsContent() {
        WindowLogger.get().addLogLine(this.getPaymentsContent());
    }

    public boolean payerHasEnoughFunds(Payment paymentToBeVerified) {
        int balance = Constants.INITIAL_MONEY;
        for (int i = 0; i < this.records.size(); i++) {
            if (this.records.get(i).getPayer().getName().equals(paymentToBeVerified.getPayer().getName())) {
                balance -= this.records.get(i).getValue();
            }
            if (this.records.get(i).getPayee().getName().equals(paymentToBeVerified.getPayer().getName())) {
                balance += this.records.get(i).getValue();
            }
        }
        if (balance >= (paymentToBeVerified.getValue() + Constants.MINING_REWARD)) {
            WindowLogger.get().addLogLine("SALDO SUFICIENTE - O saldo de " + paymentToBeVerified.getPayer().getName() + " é de: " + balance);
        } else {
            WindowLogger.get().addLogLine("SALDO INSUFICIENTE - O saldo de " + paymentToBeVerified.getPayer().getName() + " é de: " + balance);
        }
        return balance >= (paymentToBeVerified.getValue() + Constants.MINING_REWARD);
    }

    public int getEntityBalance(String name) {
        int balance = Constants.INITIAL_MONEY;
        int paymentsCount = 0;
        for (int i = 0; i < this.records.size(); i++) {
            if (this.records.get(i).getPayer().getName().equals(name)) {
                balance -= this.records.get(i).getValue();
                paymentsCount++;
            }
            if (this.records.get(i).getPayee().getName().equals(name)) {
                balance += this.records.get(i).getValue();
                paymentsCount++;
            }
        }
        WindowLogger.get().addLogLine("O saldo de " + name + " é de: " + balance + ". Pagamentos relacionados encontrados: " + paymentsCount);
        return balance;
    }

    public boolean paymentAlreadyExist(Payment payment) {
        for (Payment record : this.records) {
            if (record != null) {
                if (record.equals(payment)) {
                    return true;
                }
            }
        }
        return false;
    }

}
