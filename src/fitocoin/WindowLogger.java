/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitocoin;

import javax.swing.JTextArea;

/**
 *
 * @author Tito
 */
public class WindowLogger {

    private WindowLogger() {
    }

    public static WindowLogger get() {
        return LoggerHolder.INSTANCE;
    }

    private static class LoggerHolder {

        private static final WindowLogger INSTANCE = new WindowLogger();
    }

    private JTextArea logField;

    public void setLogField(JTextArea logField) {
        this.logField = logField;
    }

    public void cleanLogField() {

    }

    public void addLogLine(String newLine) {
        String oldLog = logField.getText();
        logField.setText(oldLog + "\n" + newLine);
    }
    
    public String getLog(){
        return this.logField.getText();
    }

}
