package fitocoin;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tiago Stapenhorst Martins
 * @author Felipe Ramon de Lara
 */
public class EntityPool {

    protected List<EntityBean> entities;

    public EntityPool() {
        entities = new ArrayList<>();
    }

    public List<EntityBean> getEntities() {
        return entities;
    }

    public void setEntities(List<EntityBean> entities) {
        this.entities = entities;
    }

    public void addEntity(EntityBean newEntity) {
        for (EntityBean bean : entities) {
            if (bean.getName().equals(newEntity.getName())) {
                return;
            }
        }
        this.entities.add(newEntity);
    }

    public String getEntityPoolString() {

        String out = "EntityPool has " + this.entities.size() + " entities.";

        for (int i = 0; i < this.entities.size(); i++) {
            //out = out.concat("\n" + this.entities.get(i).getEntityBeanString());
            out = out.concat("\n" + this.entities.get(i).getName());
        }
        return out;
    }

    public EntityBean getEntity(String name) {
        for (EntityBean ent : entities) {
            if (ent.getName().equals(name)) {
                return ent;
            }
        }
        WindowLogger.get().addLogLine("Nenhum usuário encontrado na EntityPool com este nome.");
        return null;
    }

    public EntityBean typeTheEntity() {
        String nome;
        EntityBean bean = null;
        java.util.Scanner scanner = new java.util.Scanner(System.in);
        do {
            System.out.println("Digite o nome do usuário desejado:");
            nome = scanner.nextLine().trim();
            bean = getEntity(nome);
        } while (bean == null);
        return bean;
    }

    public void removeEntity(String name) {
        for (int i = 0; i < this.entities.size(); i++) {
            if (entities.get(i).getName().equals(name)) {
                entities.remove(i);
                return;
            }
        }
    }

}
