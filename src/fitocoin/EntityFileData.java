/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitocoin;

import java.io.Serializable;
import java.security.KeyPair;
import java.util.ArrayList;
import java.util.List;
import javax.crypto.SecretKey;

/**
 *
 * @author Tito
 */
public final class EntityFileData implements Serializable {

    protected KeyPair keys;
    protected SecretKey secretKey;
    protected EntityBean entity;
    protected List<Payment> ownTransactions;
    protected Log ownLog;

    public EntityFileData(Entity en) {
        keys = en.keys;
        secretKey = en.secretKey;
        entity = en.getBean();
        ownLog = en.ownLog;
        ownTransactions = new ArrayList<>();
        for (Payment p : en.paymentRecords.getRecords()) {
            addPayment(p);
        }
    }

    public void addPayment(Payment paym) {
        //Check if the list element is related to this entity.

        if (this.entity.pkey.equals(paym.getPayee().pkey)
                || this.entity.pkey.equals(paym.getPayer().pkey)) {
            for (int i = 0; i < ownTransactions.size(); i++) {
                if (ownTransactions.get(i).equals(paym)) {
                    System.out.println("Payment is already in.");
                    return;
                }
            }
            this.ownTransactions.add(paym);
        }

    }

    public KeyPair getKeys() {
        return keys;
    }

    public SecretKey getSecretKey() {
        return secretKey;
    }

    public EntityBean getEntity() {
        return entity;
    }

    public List<Payment> getOwnTransactions() {
        return ownTransactions;
    }

    public Log getOwnLog() {
        return ownLog;
    }

}
