package fitocoin;

import java.net.UnknownHostException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import org.apache.commons.lang3.SerializationUtils;

/**
 * @author Tiago Stapenhorst Martins
 * @author Felipe Ramon de Lara
 */
public class FitoCoinV2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws UnknownHostException, NoSuchAlgorithmException {
        EntityBean e1;
        EntityBean e2;
        e1 = new EntityBean();
        e2 = new EntityBean();

        e1.setIp("localhost");
        e2.setIp("localhost");

        e1.name = "Potato";
        e2.name = "Potato";

        System.out.println(e1 == e2);
        System.out.println(e1.equals(e2));

        KeyPair keys;
        PublicKey pkey;

        KeyPairGenerator kpg = KeyPairGenerator.getInstance(Constants.ASYMMETRIC_CYPHER_ALGORITHM);
        kpg.initialize(Constants.CRYPTO_KEY_SIZE);
        keys = kpg.generateKeyPair();
        pkey = keys.getPublic();
        
        byte[] b = SerializationUtils.serialize(pkey);
        PublicKey pkey2 = (PublicKey)SerializationUtils.deserialize(b);
        
        System.out.println(pkey==pkey2);
        System.out.println(pkey.equals(pkey2));

    }
}
